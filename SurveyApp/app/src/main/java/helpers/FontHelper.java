package helpers;

import android.app.Activity;
import android.graphics.Typeface;

/**
 * Created by user on 8/25/2015.
 */
public class FontHelper {

    Activity activity;
    private Typeface font_light;
    private Typeface font_regular;
    private Typeface font_bold;
    private Typeface font_bold_cn;
    private Typeface font_cn;

    public FontHelper(Activity activity){
        this.activity = activity;
        this.font_light = Typeface.createFromAsset(this.activity.getAssets(), "fonts/arial.ttf");
        this.font_regular = Typeface.createFromAsset(this.activity.getAssets(), "fonts/arial.ttf");
        this.font_bold = Typeface.createFromAsset(this.activity.getAssets(), "fonts/arial.ttf");
        this.font_bold_cn = Typeface.createFromAsset(this.activity.getAssets(), "fonts/arial.ttf");
        this.font_cn = Typeface.createFromAsset(this.activity.getAssets(), "fonts/arial.ttf");
    }

    public Typeface getDefaultFont(String type){
        if(type.equalsIgnoreCase("light"))
            return this.font_light;
        else if(type.equalsIgnoreCase("regular"))
            return this.font_regular;
        else if(type.equalsIgnoreCase("bold_con"))
            return this.font_bold_cn;
        else if(type.equalsIgnoreCase("con"))
            return this.font_cn;
        else
            return this.font_bold;


    }

}
