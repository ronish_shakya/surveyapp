package helpers;

import android.app.Activity;
import android.content.Intent;

import java.util.ArrayList;

import surveyapp.surveyapp.activity.BrandSalesActivity;
import surveyapp.surveyapp.activity.SelectBrandActivity;
import surveyapp.surveyapp.activity.UserActivity;

/**
 * Created by rojishshakya on 4/30/16.
 */
public class Opener {

    Activity activity;
    Intent intent;

    public Opener(Activity activity) {
        this.activity = activity;
    }

    public void UserActivity() {
        intent = new Intent(activity, UserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void SelectBrandsActivity(){
        intent = new Intent(activity, SelectBrandActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public void BrandSalesActivity(ArrayList<String> alBrands){
        intent = new Intent(activity, BrandSalesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putStringArrayListExtra("alBrands", alBrands);
        activity.startActivity(intent);
    }



}
