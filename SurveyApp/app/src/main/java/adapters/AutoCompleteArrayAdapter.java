package adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import helpers.FontHelper;
import surveyapp.surveyapp.R;

/**
 * Created by rojishshakya on 8/24/16.
 */
public class AutoCompleteArrayAdapter extends ArrayAdapter<String> {

    Context context;
    ArrayList<String> data;
    FontHelper fontHelper;

    public AutoCompleteArrayAdapter(Context context,ArrayList<String> data){
        super(context, R.layout.list_item_suggestion,data);
        this.context = context;
        this.data = data;
        fontHelper = new FontHelper((Activity)context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if(rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item_suggestion,null);
        }
        TextView tv_item = (TextView) rowView.findViewById(R.id.tv_item);
        tv_item.setText(getItem(position).toString());
        tv_item.setTypeface(fontHelper.getDefaultFont("regular"));
        return rowView;
    }
}
