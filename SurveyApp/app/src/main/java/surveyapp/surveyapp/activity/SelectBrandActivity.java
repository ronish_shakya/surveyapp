package surveyapp.surveyapp.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import helpers.FontHelper;
import helpers.Opener;
import surveyapp.surveyapp.R;

/**
 * Created by rojishshakya on 8/25/16.
 */
public class SelectBrandActivity extends AppCompatActivity implements View.OnClickListener{

    Activity activity;
    FontHelper fontHelper;
    Opener opener;
    ArrayList<String> alBrands;

    TextView tv_please_select_brands;
    TextView tv_title;
    TextView tv_slk;
    TextView tv_slt;
    TextView tv_shr;
    TextView tv_kkf;
    TextView tv_plt;
    TextView tv_shr_select;
    ImageView iv_back;
    ImageView iv_done;
    CheckBox cb_slk;
    CheckBox cb_slt;
    CheckBox cb_shr;
    CheckBox cb_kkf;
    CheckBox cb_plt;
    CheckBox cb_shr_select;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_brands);

        activity = this;
        fontHelper = new FontHelper(activity);
        opener = new Opener(activity);
        alBrands = new ArrayList<>();

        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.blue_light));

        tv_please_select_brands = (TextView) findViewById(R.id.tv_please_select_brands);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_slk = (TextView) findViewById(R.id.tv_slk);
        tv_slt = (TextView) findViewById(R.id.tv_slt);
        tv_shr = (TextView) findViewById(R.id.tv_shr);
        tv_kkf = (TextView) findViewById(R.id.tv_kkf);
        tv_plt = (TextView) findViewById(R.id.tv_plt);
        tv_shr_select = (TextView) findViewById(R.id.tv_shr_select);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_done = (ImageView) findViewById(R.id.iv_done);
        cb_slk = (CheckBox) findViewById(R.id.cb_slk);
        cb_slt = (CheckBox) findViewById(R.id.cb_slt);
        cb_shr = (CheckBox) findViewById(R.id.cb_shr);
        cb_kkf = (CheckBox) findViewById(R.id.cb_kkf);
        cb_plt = (CheckBox) findViewById(R.id.cb_plt);
        cb_shr_select = (CheckBox) findViewById(R.id.cb_shr_select);

        cb_slk.setOnCheckedChangeListener(chkListener);
        cb_slt.setOnCheckedChangeListener(chkListener);
        cb_shr.setOnCheckedChangeListener(chkListener);
        cb_kkf.setOnCheckedChangeListener(chkListener);
        cb_plt.setOnCheckedChangeListener(chkListener);
        cb_shr_select.setOnCheckedChangeListener(chkListener);

        iv_back.setOnClickListener(this);
        tv_title.setOnClickListener(this);
        iv_done.setOnClickListener(this);

        tv_please_select_brands.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_title.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_slk.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_slt.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_shr.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_kkf.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_plt.setTypeface(fontHelper.getDefaultFont("regular"));
        tv_shr_select.setTypeface(fontHelper.getDefaultFont("regular"));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;

            case R.id.tv_title:
                finish();
                break;

            case R.id.iv_done:
                if(!alBrands.isEmpty()) {
                    opener.BrandSalesActivity(alBrands);
                }
                break;
        }
    }

    private CheckBox.OnCheckedChangeListener chkListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.cb_slk:
                    if(isChecked) {
                        alBrands.add("Surya Luxury Kings [SLK]");
                    }else{
                        alBrands.remove("Surya Luxury Kings [SLK]");
                    }
                    break;

                case R.id.cb_slt:
                    if(isChecked) {
                        alBrands.add("Surya Lights [SLT]");
                    }else{
                        alBrands.remove("Surya Lights [SLT]");
                    }
                    break;

                case R.id.cb_shr:
                    if(isChecked) {
                        alBrands.add("Shikhar [SHR]");
                    }else{
                        alBrands.remove("Shikhar [SHR]");
                    }
                    break;

                case R.id.cb_kkf:
                    if(isChecked) {
                        alBrands.add("Khukuri Filter [KKF]");
                    }else{
                        alBrands.remove("Khukuri Filter [KKF]");
                    }
                    break;

                case R.id.cb_plt:
                    if(isChecked) {
                        alBrands.add("Pilot [PLT]");
                    }else{
                        alBrands.remove("Pilot [PLT]");
                    }
                    break;

                case R.id.cb_shr_select:
                    if(isChecked) {
                        alBrands.add("Shikhar Select [SHR Select]");
                    }else{
                        alBrands.remove("Shikhar Select [SHR Select]");
                    }
                    break;
            }
        }
    };
}
