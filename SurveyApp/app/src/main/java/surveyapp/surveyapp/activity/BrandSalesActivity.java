package surveyapp.surveyapp.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;

import helpers.FontHelper;
import helpers.Opener;
import surveyapp.surveyapp.R;

/**
 * Created by rojishshakya on 7/23/16.
 */
public class BrandSalesActivity extends AppCompatActivity implements View.OnClickListener{

    Activity activity;
    FontHelper fontHelper;
    Opener opener;

    RelativeLayout rl_mainView;
    ImageView iv_back;
    TextView tv_title;
    ImageView iv_done;
    ImageView iv_settings;
    ImageView iv_add;
    LinearLayout ll_wrapper;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_brandsales);

        rl_mainView = (RelativeLayout) findViewById(R.id.rl_mainView);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_done = (ImageView) findViewById(R.id.iv_done);
        iv_settings = (ImageView) findViewById(R.id.iv_settings);
        iv_add = (ImageView) findViewById(R.id.iv_add);
        ll_wrapper = (LinearLayout) findViewById(R.id.ll_wrapper);
        setupUI(rl_mainView);

        activity = this;
        fontHelper = new FontHelper(activity);
        opener = new Opener(activity);

        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.blue_light));

        Intent i = getIntent();
        ArrayList<String> alBrands = i.getStringArrayListExtra("alBrands");

        for(String brand : alBrands){
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = inflater.inflate(R.layout.brand_item_records, null, false);
            BrandHolder brandHolder = new BrandHolder();
            brandHolder.tvBrandName = (TextView) convertView.findViewById(R.id.tv_brand_name);
            brandHolder.etOS = (EditText) convertView.findViewById(R.id.et_os);
            brandHolder.etP = (EditText) convertView.findViewById(R.id.et_p);
            brandHolder.etCS = (EditText) convertView.findViewById(R.id.et_cs);

            brandHolder.tvBrandName.setText(brand);
            brandHolder.tvBrandName.setTypeface(fontHelper.getDefaultFont("regular"));
            brandHolder.etOS.setTypeface(fontHelper.getDefaultFont("regular"));
            brandHolder.etP.setTypeface(fontHelper.getDefaultFont("regular"));
            brandHolder.etCS.setTypeface(fontHelper.getDefaultFont("regular"));

            ll_wrapper.addView(convertView);

        }

        iv_back.setOnClickListener(this);
        tv_title.setOnClickListener(this);
        iv_done.setOnClickListener(this);
        iv_settings.setOnClickListener(this);
        iv_add.setOnClickListener(this);


        tv_title.setTypeface(fontHelper.getDefaultFont("regular"));

    }

    /**
     * Method for removing the keyboard if touched outside the editview.
     *
     * @param view
     */
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    hideSoftKeyboard();
                    return false;
                }
            });


        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.iv_back:
                finish();
                break;

            case R.id.tv_title:
                finish();
                break;

            case R.id.iv_done:
                Toast.makeText(activity,"Work on progress!",Toast.LENGTH_SHORT).show();
                break;

            case R.id.iv_settings:
                opener.UserActivity();
                break;

            case R.id.iv_add:
                finish();
                break;

        }
    }

    private class BrandHolder {

        private TextView tvBrandName;
        private EditText etOS;
        private EditText etP;
        private EditText etCS;

        int position;
    }
}
