package surveyapp.surveyapp.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import adapters.AutoCompleteArrayAdapter;
import helpers.FontHelper;
import helpers.Opener;
import surveyapp.surveyapp.R;

/**
 * Created by rojishshakya on 7/18/16.
 */
public class UserActivity extends AppCompatActivity implements View.OnClickListener{

    RelativeLayout rl_mainView;
    AutoCompleteTextView at_outlet;
    AutoCompleteTextView at_route;
    AutoCompleteTextView at_area;
    Button btn_set;

    Activity activity;
    FontHelper fontHelper;
    Opener opener;

    AutoCompleteArrayAdapter adapter_area;
    AutoCompleteArrayAdapter adapter_route;
    AutoCompleteArrayAdapter adapter_outlet;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        rl_mainView = (RelativeLayout) findViewById(R.id.rl_mainView);
        at_outlet = (AutoCompleteTextView) findViewById(R.id.at_outlet);
        at_route = (AutoCompleteTextView) findViewById(R.id.at_route);
        at_area = (AutoCompleteTextView) findViewById(R.id.at_area);
        btn_set = (Button) findViewById(R.id.btn_set);

        activity = this;
        fontHelper = new FontHelper(this);
        opener = new Opener(this);
        setupUI(rl_mainView);

        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.blue_light));


        ArrayList<String> alArea = new ArrayList<>();
        alArea.add("Pulchowk");
        alArea.add("Kupondole");
        alArea.add("Thamel");
        alArea.add("Sanepa");
        alArea.add("Dhapakhel");
        alArea.add("Thapathali");
        alArea.add("Tripureshwor");
        alArea.add("Kalanki");
        alArea.add("Kalimati");
        alArea.add("Putalisadak");
        alArea.add("Kapan");
        alArea.add("Ktest");
        alArea.add("Ktest1");
        alArea.add("Ktest2");
        alArea.add("Ktest3");



        adapter_area = new AutoCompleteArrayAdapter(this,alArea);
        at_area.setAdapter(adapter_area);

        ArrayList<String> alRoute = new ArrayList<>();
        alRoute.add("Route1");
        alRoute.add("Route2");
        alRoute.add("Route3");
        alRoute.add("Route4");
        alRoute.add("Route5");
        alRoute.add("TestRoute1");
        alRoute.add("TestRoute2");

        adapter_route = new AutoCompleteArrayAdapter(this,alRoute);
        at_route.setAdapter(adapter_route);

        ArrayList<String> alOutlet = new ArrayList<>();
        alOutlet.add("Outlet1");
        alOutlet.add("Outlet2");
        alOutlet.add("Outlet3");
        alOutlet.add("Outlet4");
        alOutlet.add("TestOutlet1");
        alOutlet.add("TestOutlet2");

        adapter_outlet = new AutoCompleteArrayAdapter(this,alOutlet);

        at_outlet.setAdapter(adapter_outlet);


        btn_set.setOnClickListener(this);

        at_outlet.setTypeface(fontHelper.getDefaultFont("regular"));
        at_route.setTypeface(fontHelper.getDefaultFont("regular"));
        at_area.setTypeface(fontHelper.getDefaultFont("regular"));
        btn_set.setTypeface(fontHelper.getDefaultFont("regular"));
    }

    /**
     * Method for removing the keyboard if touched outside the editview.
     *
     * @param view
     */
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    hideSoftKeyboard();
                    return false;
                }
            });


        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_set:
                String area = at_area.getText().toString();
                String route = at_route.getText().toString();
                String outlet = at_outlet.getText().toString();

                if(!outlet.isEmpty() && !route.isEmpty() && !area.isEmpty()) {

                    //save these in shared preference
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("area", area);
                    editor.putString("route", route);
                    editor.putString("outlet", outlet);

                    editor.commit();

                    opener.SelectBrandsActivity();
//                    opener.BrandSalesActivity();
                }else{
                    Toast.makeText(activity,"Please complete all the fields.",Toast.LENGTH_SHORT).show();
                }

//                opener.BrandSalesActivity();

                break;
        }
    }

}
